<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 10/10/2016
 * Time: 11:39 AM
 */
    function oddNumber($var){

        return($var & 1);
    }

    function evenNumber($var){

        return(!($var & 1));
    }

    $array1=array("a"=>1,"b"=>2,"c"=>3,"d"=>4,"e"=>5,"f"=>6);

    echo "Odd:\n";
    echo "<pre>";
    print_r(array_filter($array1, "oddNumber"));
    echo "</pre>";
    echo "Even:\n";
    echo "<pre>";
    print_r(array_filter($array1, "evenNumber"));
    echo "</pre>";

?>

<?php

    $base = array("orange", "banana", "apple", "raspberry");
    $replacements = array(0 => "pineapple", 4 => "cherry");
    $replacements2 = array(0 => "grape");

    $basket = array_replace($base, $replacements, $replacements2);

    print_r($base);
    echo "<br>";
    print_r($basket);
?>
