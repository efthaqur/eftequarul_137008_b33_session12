<?php
/**
 * Example of Nested array
 *
 * Date: 10/10/2016
 * Time: 10:01 AM
 */

    $number=array(88,55,array('PHP','HTML','CSS'));

    foreach($number as $value){

        if(is_array($value))
            foreach($value as $nested_value){
                echo $nested_value."<br>";
            }
        else {
            echo $value."<br>";
        }
    }




